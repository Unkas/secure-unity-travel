require 'rails_helper'

RSpec.describe HotelTag, type: :model do
  it { should validate_presence_of :hotel_id }
  it { should validate_presence_of :tag_id }

  describe 'dependencies' do
    it { should belong_to(:hotel) }
    it { should belong_to(:tag) }
  end
end
