FROM ruby:2.2

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs libmagickwand-dev imagemagick graphviz

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

RUN gem install bundler -v 1.17.3

COPY Gemfile /usr/src/app/Gemfile
COPY Gemfile.lock /usr/src/app/Gemfile.lock

RUN bundle install
COPY . /usr/src/app

CMD puma -C config/puma.rb
