class Hotel < ActiveRecord::Base

  has_many :hotel_tags, dependent: :destroy
  has_many :tags, through: :hotel_tags

  validates_presence_of :name
end
