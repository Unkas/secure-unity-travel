require 'rails_helper'

RSpec.feature "Hotels::index-show", type: :feature do

  context 'when we click on hotel in list' do
    scenario "all works" do
      visit hotels_path

      see_all_hotels?

      all_the_hotels.first.find('img').click

      goto_first_hotel_page

      expect(current_path).to eq "/hotels/1"
      expect(page).to have_content Hotel.find(1).name
    end

    def all_the_hotels
      find_all('.hotel-wrapper')
    end

    def see_all_hotels?
      expect(all_the_hotels.count).to eq Hotel.count
    end

    def goto_first_hotel_page
      all_the_hotels.first.find_all('a').first.click
    end
  end
end
