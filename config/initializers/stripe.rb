Rails.configuration.stripe = {
  :publishable_key => STRIPE_CONFIG[:PUBLISHABLE_KEY],
  :secret_key      => STRIPE_CONFIG[:SECRET_KEY]
}

Stripe.api_key = Rails.configuration.stripe[:secret_key]
Stripe.api_version = "2019-12-03"