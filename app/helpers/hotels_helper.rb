module HotelsHelper

  def color_scheme(tag)
    case tag.color_scheme

    when 1
      'danger'
    when 2
      'warning'
    when 3
      'success'
    end
  end

  def star_class(star_number, hotel)
    if hotel.rating.round >= star_number
      'glyphicon-star text-gold'
    else
      'glyphicon-star-empty text-gray-600'
    end
  end
end
