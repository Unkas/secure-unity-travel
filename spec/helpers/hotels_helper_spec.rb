require 'rails_helper'

RSpec.describe HotelsHelper, type: :helper do
  let(:tag_1) { Tag.find 1 }
  let(:tag_2) { Tag.find 2 }
  let(:tag_3) { Tag.find 3 }

  let(:hotel_1) { Hotel.find 1 }

  describe "#title" do
    it "returns color scheme" do
      expect(helper.color_scheme(tag_1)).to eq 'danger'
      expect(helper.color_scheme(tag_2)).to eq 'success'
      expect(helper.color_scheme(tag_3)).to eq 'warning'
    end

    it "returns star_class" do
      expect(helper.star_class(3, hotel_1)).to eq 'glyphicon-star text-gold'
      expect(helper.star_class(5, hotel_1)).to eq 'glyphicon-star-empty text-gray-600'
    end
  end
end
