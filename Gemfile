source 'https://rubygems.org'
ruby '2.2.10'

gem 'bcrypt'

gem 'rails', '4.0.13' # https://github.com/rails/rails/issues/13648#issuecomment-36441503

gem 'will_paginate'

gem 'jquery-rails'
gem 'jquery-ui-rails'

gem 'font-awesome-sassc', git: 'https://github.com/partounian/font-awesome-sassc.git'
gem 'sassc-rails'

gem 'bootstrap-multiselect-rails'
gem 'bootstrap-sass'
gem 'bootstrap-slider-rails'
gem 'bootstrap-will_paginate'
gem 'flex-slider-rails'

gem 'jquery-cookie-rails'
gem 'jquery-tablesorter'
gem 'remotipart', '1.4.3' # Rails jQuery file uploads via standard Rails "remote: true" forms. (http://os.alfajango.com/remotipart)
# necessary for best_in_place
gem 'best_in_place', '2.1.0', git: 'https://github.com/aaronchi/best_in_place.git'
gem 'coffee-rails'

gem 'stripe', '~> 4.24'

gem 'uglifier'
gem 'wiselinks'

gem 'rails-patch-json-encode'
gem 'oj'

gem 'angularjs-rails', '1.2.26'

gem 'filepicker-rails'

gem 'spreadsheet'
gem 'time_diff'

gem 'roo', '2.7.1'
gem 'sitemap_generator'

gem 'sendgrid' # unofficial gem

gem 'draper'

gem 'aws-sdk-v1'

gem 'country_select'
gem 'wannabe_bool'

gem 'slack-poster'

gem 'puma'
gem 'puma-heroku'

gem 'slowpoke'

gem 'awesome_print'
gem 'lograge'

gem 'pg', '~> 0.11'

group :production do
  gem 'redis', '~> 3.3'

  # "must haves"
  gem 'heroku-deflater'
  gem 'rails_12factor'

  # redis
  gem 'redis-rack-cache'
  gem 'redis-rails'

  # apms
  # gem 'newrelic_rpm'
  # gem 'scout_apm' only works on MRI
end

group :test do
  gem 'capybara'
  gem 'simplecov'
  gem 'xpath'
  gem 'stripe-ruby-mock'
  gem 'simplecov', require: false
  gem 'database_cleaner-active_record'
  gem 'shoulda-matchers', '2.5.0'
  gem 'factory_bot'
end

group :test, :development do
  gem 'ffaker'
  gem 'test-unit' # useful for ruby 2.2+(?)
  gem 'rspec-rails'
  gem 'pry-rails'
end

group :development do
  gem 'solargraph'
  # breaks the code(?)
  gem 'rubocop'
  gem 'rubocop-rspec'
  # gem 'rubocop-rails' # for use with ruby 2.3+
  # gem 'rubocop-performance' # for use with ruby 2.3+

  gem 'rails_stdout_logging'

  gem 'quiet_assets'

  # gem 'pry-rails'

  gem 'better_errors'
  # gem 'meta_request'
  # gem 'rack-mini-profiler', '1.0.0'
  gem 'web-console', '~> 2.0'
end
