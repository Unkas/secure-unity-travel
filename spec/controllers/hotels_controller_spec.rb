require 'rails_helper'

RSpec.describe HotelsController, type: :controller do

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:hotels)).to match_array Hotel.all
    end
  end

  describe "GET #show" do
    let (:hotel_1) { Hotel.find 1 }
    it "returns http success" do
      get :show, id: hotel_1.id
      expect(response).to have_http_status(:success)
      expect(assigns(:hotel)).to eq  hotel_1
    end
  end
end
