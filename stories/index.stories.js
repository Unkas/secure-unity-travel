import '../app/assets/stylesheets/bootstrap-iso.css';
import '../app/assets/stylesheets/bootstrap-iso-custom.css';
import {addDecorator} from '@storybook/html';

export default {
  title: 'Demo',
};

const storyAsString = (story) => `<div class="bootstrap-iso">${story}</div>`;
const storyAsNode = (story) => {
  const wrapper = document.createElement('div');
  wrapper.className = 'bootstrap-iso';
  wrapper.appendChild(story);
  return wrapper;
};

addDecorator(story => {
  const tale = story();
  return typeof tale === "string" ? storyAsString(tale) : storyAsNode(tale);
});

export const Header = () => `
<nav class="navbar navbar-default bg-gray-100 border-none">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand p-1 pl-20" href="/">
        <img class="h-full" alt="Unity Travel logo" src="/images/site-logo-unity.png">
      </a>
    </div>
    <ul class="nav navbar-nav navbar-right pr-20">
      <li class="navbar-text itext-gray-300">Currency</li>
      <li class="dropdown">
        <button class="btn btn-default navbar-btn dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
          USD
          <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="#">USD</a></li>
          <li><a href="#">CAD</a></li>
        </ul>
      </li>
      <li class="navbar-text m-0 py-2">
        <span class="fa-stack fa-lg">
          <i class="fas fa-circle fa-stack-2x text-blue-700"></i>
          <i class="fas fa-bell fa-stack-1x fa-inverse"></i>
        </span>
      </li>
      <li class="navbar-text m-0 py-2">
        <span class="fa-stack fa-lg">
          <i class="fas fa-circle fa-stack-2x text-blue-700"></i>
          <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i>
        </span>
      </li>
      <li class="dropdown text-blue-700">
        <a href="#" class="dropdown-toggle ipl-1" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Patrick Artounian<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">My Profile</a></li>
          <li><a href="#">Settings</a></li>
          <li><a href="#">Logout</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
`;

// node example
// export const Button = () => {
//   const btn = document.createElement('button');
//   btn.type = 'button';
//   btn.innerText = 'Hello Button';
//   btn.className = "btn btn-primary";
//   btn.addEventListener('click', e => console.log(e));
//   return btn;
// };

export const ResultsFilterBar = () => `
<div class="container-fluid">
  <form class="form-inline">
    <div class="row">
      <div class="form-group">
        <input type="search" class="form-control" placeholder="Search hotels around the festival">
      </div>
      <div class="form-group">
        <label>Check In</label>
        <input type="date" class="form-control">
      </div>
      <div class="form-group">
        <label>Check In</label>
        <input type="date" class="form-control">
      </div>
      <div class="form-group">
        <label>Rooms</label>
        <input type="number" class="form-control">
      </div>
      <button type="submit" class="btn btn-success">Filters</button>
      <button type="submit" class="btn btn-primary">Search</button>
    </div>
  </form>
</div>
`;

export const ResultsNumberAndSortByBar = () => `
<div class="container-fluid mb-1">
  <div class="row vertical-align">
    <div class="col-xs-6">
      # places found
    </div>
    <div class="col-xs-6 text-right">
      Sort by
      <select>
        <option value="price">Price</option>
        <option value="distance">Distance</option>
      </select>
    </div>
  </div>
</div>
`;

export const HotelCard = () =>
  `<div class="panel panel-default">
    <div class="panel-body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-4 col-md-3">
            <a href="#" class="thumbnail border-none">
              <img src="https://upload.wikimedia.org/wikipedia/commons/b/b6/Dakota-Hotel-WEB.jpg" alt="HOTEL_NAME thumbnail" width="150" height="150">
            </a>
          </div>
          <div class="col-xs-8 col-md-9">
            <!-- tags ex: Featured Free parking -->
            <span class="label label-danger">Featured</span>
            <span class="label label-success">Free parking</span>
            <div class="row">
              <div class="col-xs-8">
                <h4>
                  Sura Design Hotel &amp; Suites
                </h4>
              </div>
              <div class="col-xs-4">
                <h4 class="pull-right">$XX.XX <small class="text-gray-650">CAD</small></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <span class="glyphicon glyphicon-star text-lg text-gold" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star text-lg text-gold" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star text-lg text-gold" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star text-lg text-gold" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star-empty text-lg text-gray-600" aria-hidden="true"></span>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-8">
                <span class="glyphicon glyphicon-map-marker text-3xl text-blue-500" aria-hidden="true"></span>
                <span class="text-gray-700 text-sm">Divan Yolu Cad. Ticarethane Sok. No 43 Sultanahmet, Fatih, 34110 Istanbul, Turkey</span>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <button role="button" class="btn btn-primary pull-right">See availability »</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>`;

export const Footer = () => `
<div class="bg-blue-700 text-white py-6">
  <div class="row pb-4 text-center">
    <img class="h-full" alt="Unity Travel logo" src="/images/site-logo-unity.png">
  </div>
  <div class="row uppercase pb-4 text-center"> 
    <div class="col-md-4">
      Privacy Policy
    </div>

    <div class="col-md-4">
      Terms &amp; Conditions
    </div>

    <div class="col-md-4">
      Cancellation Policy
    </div>
  </div>
  <div class="row text-center"> 
     © 2020, Unity Travel, LLC. All rights reserved
  </div>
</div>
`;

export const HotelResultsPage = () => `
${Header()}
<h1 class="text-center">Electric Weekend 2020</h1>
<div class="container border-b-d mb-1">
  <img src="https://www.unity.travel/app/uploads/2017/12/ELECTRIC-WEEKEND-2020-1.png" style="width:100%">
  <p>Book your Electric Weekend with us! Hotel Rooms are available for over $200 below market rate. Start a Payment Plan or make Split Payments with friends!</p>
  <p>CONTACT INFO@UNITY.TRAVEL FOR QUESTIONS AND INFORMATION!</p>
</div>
<div class="container">
  <ul class="nav nav-tabs" role="tablist">
    <li class="bg-blue-300-op-20 rounded-t text-center"><a href="access_pane">Access</a></li>
    <li class="active rounded-t text-center w-24"><a href="hotel_pane">Hotels</a></li>
  </ul>
  <div class="tab-content bg-white border border-gray-200 p-8">
    <div class="tab-pane" id="access_pane"></div>
    <div class="tab-pane active" id="hotel_pane">
      ${ResultsFilterBar()}
      <hr>
      ${ResultsNumberAndSortByBar()}
      ${HotelCard()}
      ${HotelCard()}
      ${HotelCard()}
      ${HotelCard()}
    </div>
  </div>
</div>
<div class="h-16"></div>
${Footer()}
`;
