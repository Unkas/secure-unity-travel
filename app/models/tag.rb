class Tag < ActiveRecord::Base

  has_many :hotel_tags, dependent: :destroy
  has_many :hotels, through: :hotel_tags

  before_validation :downcase_name

  validates_presence_of :name

  def downcase_name
    self.name = name.to_s.downcase
  end
end
