JusCollege::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # config.assets.css_compressor = :sass
  # config.assets.js_compressor = Uglifier.new(:mangle => false)

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true

  # specify what domain to use for mailer URLs
  config.action_mailer.default_url_options = { host: 'localhost:3000' }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :stdout

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  # useful when debugging CSS/JS
  config.assets.debug = false

  config.log_level = :debug

  config.web_console.whitelisted_ips = ['172.18.0.0/12', '192.168.0.0/16', '127.0.0.1', '10.0.0.0/8']
end
