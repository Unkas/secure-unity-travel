# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# Create tags
tag_1 = Tag.find_or_create_by(name: 'featured', color_scheme: 1)
tag_2 = Tag.find_or_create_by(name: 'free parking', color_scheme: 3)
tag_3 = Tag.find_or_create_by(name: 'all inclusive', color_scheme: 2)

# Create hotels
hotel_1 = Hotel.find_or_create_by(
  name: 'Sura Design Hotel & Suites',
  rating: 3.25,
  price: 111.11,
  address: ' Divan Yolu Cad. Ticarethane Sok. No 43 Sultanahmet, Fatih, 34110 Istanbul, Turkey',
  image_address: 'https://upload.wikimedia.org/wikipedia/commons/b/b6/Dakota-Hotel-WEB.jpg'
  )
hotel_2 = Hotel.find_or_create_by(
  name: 'Hilton',
  rating: 4.25,
  price: 222.22,
  address: 'Triangeln 2, 211 43 Malmö, Sweden',
  image_address: 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/Hilton_Malm%C3%B6_City.jpg/165px-Hilton_Malm%C3%B6_City.jpg'
  )
hotel_3 = Hotel.find_or_create_by(
  name: 'Soho club',
  rating: 4.85,
  price: 333.33,
  address: '16, Juhu Tara Rd, Juhu Tara, Santacruz West, Mumbai, Maharashtra 400049, India',
  image_address: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Sohohouse1.JPG/220px-Sohohouse1.JPG'
  )

# Create hotel_tags
HotelTag.find_or_create_by(hotel_id: hotel_1.id, tag_id: tag_1.id)
HotelTag.find_or_create_by(hotel_id: hotel_1.id, tag_id: tag_2.id)

HotelTag.find_or_create_by(hotel_id: hotel_2.id, tag_id: tag_2.id)
HotelTag.find_or_create_by(hotel_id: hotel_2.id, tag_id: tag_3.id)

HotelTag.find_or_create_by(hotel_id: hotel_3.id, tag_id: tag_3.id)
HotelTag.find_or_create_by(hotel_id: hotel_3.id, tag_id: tag_1.id)