# README - Unity.Travel


### SETUP AND RUN:

    #### [RECOMMENDED] DOCKER:
        Install Docker & Docker Compose

        NOTE: If you are using Windows add a -f docker-compose.windows.yml to every docker-compose command

        Run docker:
            * build docker: docker-compose build
            * start docker: docker-compose up

        Create and setup database:
            * docker-compose run web rake db:create
            * docker-compose run web rake db:migrate
            * docker-compose run web rake db:seed

        Open website at localhost:3000

        Other commands:
            * run unit tests: docker-compose run -e RAILS_ENV=test web bash -c 'rake db:migrate && rspec spec'
                * I recommend using `--fail-fast` for rspec
            * stop docker: docker-compose down
            * docker commands: docker-compose run web <commandline>
                + Eg: docker-compose run web bundle install

    #### RUN DIRECTLY (MAC / LINUX):

        Install Rails & Database:
            * Install Rails: https://gorails.com/setup/osx/10.13-high-sierra
            * Install PostgreSQL & PGAdmin 3 or 4 tool: https://www.pgadmin.org/download/

        Run website:
            * bundle install
            * rake db:create  (to create database)
            * rake db:migrate (to generate database schemas)
            * rake db:seed    (to generate sample data)
            * open website:   http://localhost:3000

    #### RUN IN PRODUCTION MODE:

        * RAILS_ENV=production rails assets:precompile
        * RAILS_ENV=production rails s -e production -p 80 -d


### RUN UNIT TESTS:

    * run all tests:    rspec spec
    * model tests:      rspec spec/unit_tests/models
