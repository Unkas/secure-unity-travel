﻿var mongoose = require('mongoose'),
    orm = require("orm"),
    Q = require('q'),
    async = require('async'),
    //postgresConnectionString = 'postgres://postgres:postgres@192.168.72.128:5432/juscollege_migration',
    //mongoConnectionString = 'mongodb://192.168.72.128:27017/juscollege_development',
    postgresConnectionString = 'postgres://postgres:123456@localhost:5432/juscollege_migration',
    mongoConnectionString = 'mongodb://localhost:27017/juscollege_development',
    //mongoConnectionString = 'mongodb://heroku_app9276994:h23kj71mt6jh80aoi7lkl61h2l@ds045877.mongolab.com:45877/heroku_app9276994',
    mongoDb;

mongoose.connect(mongoConnectionString);
mongoDb = mongoose.connection;
mongoDb.on('error', console.error.bind(console, 'connection error:'));

async.parallel([
    function (callback) {
        orm.connect(postgresConnectionString, function (err, db) {
            callback(err, db);
        });
    },
    function (callback) {
        mongoDb.once('open', function () {
            callback();
        });
    }
], function (err, results) {
    var postgresDb;

    if (err) {
        throw err;
    }

    postgresDb = results[0];

    var eventSchema = mongoose.Schema({
        _id: String,
        affiliate_master_id: String,
        created_at: Date,
        event_access: Boolean,
        event_active: Boolean,
        event_address_1: String,
        event_address_2: String,
        event_buster: Boolean,
        event_city: String,
        event_country: String,
        event_deadline: Date,
        event_description: String,
        event_end_date: String,
        event_end_date_data_type: Date,
        event_end_time: String,
        event_image_1: String,
        event_latitude: String,
        event_longitude: String,
        event_no_of_days: Number,
        event_pin: String,
        event_priority: Number,
        event_property_name: String,
        event_start_date: String,
        event_start_date_data_type: Date,
        event_start_time: String,
        event_state: String,
        event_visible: Boolean,
        event_weight: Number,
        event_zip: String,
        min_paid_percentage: Number,
        name: String,
        permalink: String,
        survey_link: String,
        updated_at: Date
    });

    var productSchema = mongoose.Schema({
        _id: String,
        active: Boolean,
        address: String,
        city: String,
        company_name: String,
        created_at: Date,
        embed_html: String,
        event_id: String,
        instock: Number,
        long_description: String,
        name: String,
        phone: String,
        price: Number,
        product_embed_html: Boolean,
        product_image_1: String,
        product_priority: Number,
        product_type: String,
        product_visible: Boolean,
        product_weight: Number,
        quantity: Number,
        short_description: String,
        sku: String,
        state: String,
        updated_at: Date,
        user_id: String,
        zip: String
    });

    var orderSchema = mongoose.Schema({
        _id: String,
        buyer_id: String,
        contract_id: String,
        created_at: Date,
        event_id: String,
        extra_amount: Number,
        promote_code: String,
        reduce_amount: Number,
        referral_email: String,
        splitted_amounts: [Number],
        splitted_emails: [String],
        status: String,
        total_price: Number,
        updated_at: Date
    });

    var orderItemSchema = mongoose.Schema({
        _id: String,
        quantity: Number,
        status: String,
        product_id: String,
        unit_price: Number,
        order_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Order' },
        created_at: Date,
        updated_at: Date
    });

    var orderSplitmentSchema = mongoose.Schema({
        _id: String,
        accepted: Number,
        card_code: String,
        card_holder_name: String,
        card_month: String,
        card_number: String,
        card_type: String,
        card_year: String,
        created_at: Date,
        email: String,
        order_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Order' },
        payment_method: String,
        purchase_billing_email: String,
        purchase_billing_fullname: String,
        purchase_billing_phone: String,
        purchase_diff_billing: Boolean,
        purchase_email_sent_user: Boolean,
        purchase_paid_date: Date,
        purchase_payment_id: String,
        purchase_refund_date: Date,
        purchase_refund_state: String,
        purchase_refund_trx_id: String,
        purchase_sale_id: String,
        sent_mail: Boolean,
        shared_price: Number,
        status: String,
        updated_at: Date,
        user_id: String,
        product_list: [String]
    });

    var userSchema = mongoose.Schema({
        _id: String,
        created_at: Date,
        email: String,
        fb_birthday: String,
        fb_education_college_id: String,
        fb_education_college_name: String,
        fb_education_college_year_id: String,
        fb_education_college_year_name: String,
        fb_education_high_school_id: String,
        fb_education_high_school_name: String,
        fb_education_high_school_year_id: String,
        fb_education_high_school_year_name: String,
        fb_gender: String,
        fb_hometown_id: String,
        fb_hometown_name: String,
        fb_image: String,
        fb_link: String,
        fb_locale: String,
        fb_location_id: String,
        fb_location_name: String,
        fb_nickname: String,
        fb_timezone: String,
        fb_token: String,
        fb_token_expires: String,
        fb_token_expires_at: String,
        fb_work_employer_id: String,
        fb_work_employer_name: String,
        fb_work_end_date: String,
        fb_work_location_id: String,
        fb_work_location_name: String,
        fb_work_position_id: String,
        fb_work_position_name: String,
        fb_work_start_date: String,
        first_name: String,
        last_name: String,
        name: String,
        permalink: String,
        phone: String,
        provider: String,
        role_admin: Boolean,
        role_user: Boolean,
        source_login: String,
        uid: String,
        updated_at: Date
    });

    var Event = mongoose.model('Event', eventSchema);
    var Product = mongoose.model('Product', productSchema);
    var Order = mongoose.model('Order', orderSchema);
    var OrderItem = mongoose.model('order_item', orderItemSchema);
    var OrderSplitment = mongoose.model('order_splitment', orderSplitmentSchema);
    var User = mongoose.model('User', userSchema);

    var PgUser = postgresDb.define('users', {
        first_name: { type: "text" },
        last_name: { type: "text" },
        email: { type: "text" },
        password_digest: { type: "text" },
        remember_token: { type: "text" },
        admin: { type: "boolean" },
        image_1: { type: "text" },
        active: { type: "boolean" },
        phone: { type: "text" },
        legacy_id: { type: "text" },
        has_password: { type: "boolean" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgFbinfo = postgresDb.define('fbinfos', {
        user_id: { type: "number" },
        uid: { type: "text" },
        nickname: { type: "text" },
        image: { type: "text" },
        link: { type: "text" },
        email: { type: "text" },
        name: { type: "text" },
        token: { type: "text" },
        token_expires: { type: "boolean" },
        token_expires_at: { type: "number" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        first_name: { type: "text" },
        last_name: { type: "text" },
        timezone: { type: "number" },
        birthday: { type: "text" },
        gender: { type: "text" },
        location_id: { type: "text" },
        location_name: { type: "text" },
        hometown_id: { type: "text" },
        hometown_name: { type: "text" },
        locale: { type: "text" }
    });

    var PgFbinfoEducation = postgresDb.define('fbinfo_educations', {
        fbinfo_id: { type: "number" },
        school_id: { type: "text" },
        school_name: { type: "text" },
        education_type: { type: "text" },
        year_id: { type: "text" },
        year_name: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgFbinfoWork = postgresDb.define('fbinfo_works', {
        fbinfo_id: { type: "number" },
        employer_id: { type: "text" },
        employer_name: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        position_id: { type: "text" },
        position_name: { type: "text" },
        start_date: { type: "text" },
        end_date: { type: "text" },
        location_id: { type: "text" },
        location_name: { type: "text" }
    });

    var PgEvent = postgresDb.define('events', {
        name: { type: "text" },
        slug: { type: "text" },
        description: { type: "text" },
        deadline: { type: "date" },
        start_date: { type: "date" },
        end_date: { type: "date" },
        address: { type: "text" },
        city: { type: "text" },
        zip: { type: "text" },
        country_id: { type: "number" },
        latitude: { type: "text" },
        longitude: { type: "text" },
        image_1: { type: "text" },
        images: { type: "text" },
        videos: { type: "text" },
        priority: { type: "number" },
        visible: { type: "boolean" },
        active: { type: "boolean" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        state_id: { type: "number" },
        percent_deposit: { type: "number" },
        event_type: { type: "text" },
        legacy_id: { type: "text" },
        start_time: { type: "text" },
        end_time: { type: "text" }
    });

    var PgState = postgresDb.define('states', {
        abbr: { type: "text" },
        name: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgCountry = postgresDb.define('countries', {
        name: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgVendor = postgresDb.define('vendors', {
        name: { type: "text" },
        description: { type: "text" },
        address: { type: "text" },
        city: { type: "text" },
        zip: { type: "number" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        user_id: { type: "number" },
        state_id: { type: "number" },
        slug: { type: "text" },
        country_id: { type: "number" }
    });

    var PgAddress = postgresDb.define('addresses', {
        line_1: { type: "text" },
        line_2: { type: "text" },
        city: { type: "text" },
        state_id: { type: "number" },
        zip: { type: "text" },
        country_id: { type: "number" }
    });

    var PgBaseProduct = postgresDb.define('products', {
        name: { type: "text" },
        slug: { type: "text" },
        master_price: { type: "number" },
        description: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        vendor_id: { type: "number" },
        user_id: { type: "number" },
        as_product_id: { type: "number" },
        as_product_type: { type: "text" },
        address_id: { type: "number" },
        variant_type_id: { type: "number" },
        longitude: { type: "text" },
        latitude: { type: "text" },
        active: { type: "boolean" },
        visible: { type: "boolean" }
    });

    var PgProductImage = postgresDb.define('product_images', {
        product_id: { type: "number" },
        url: { type: "text" },
        is_visible: { type: "boolean" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgProductVariant = postgresDb.define('product_variants', {
        is_auto_created: { type: "boolean" },
        product_id: { type: "number" },
        variant_id: { type: "number" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgProductBlock = postgresDb.define('product_blocks', {
        quantity: { type: "number" },
        instock: { type: "number" },
        unit_price: { type: "number" },
        unit_cost: { type: "number" },
        has_unlimited_quantity: { type: "boolean" },
        has_unlimited_dates: { type: "boolean" },
        creator_id: { type: "number" },
        active: { type: "boolean" },
        product_variant_id: { type: "number" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        using_master_calendar: { type: "boolean" }
    });

    var PgProductBlockAvailableDate = postgresDb.define('product_block_available_dates', {
        product_block_id: { type: "number" },
        date: { type: "date" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgLegacyProduct = postgresDb.define('legacy_products', {
        product_type: { type: "text" },
        company_name: { type: "text" },
        product_embed_html: { type: "boolean" },
        embed_html: { type: "text" },
        quantity: { type: "number" },
        instock: { type: "number" },
        event_id: { type: "number" },
        legacy_id: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    var PgClubAccess = postgresDb.define('club_accesses', {
        club_name: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        cost: { type: "number" }
    });

    var PgSuiteParty = postgresDb.define('suite_parties', {
        created_at: { type: "date" },
        updated_at: { type: "date" },
        cost: { type: "number" }
    });

    var PgOrder = postgresDb.define('orders', {
        event_id: { type: "number" },
        buyer_id: { type: "number" },
        total_price: { type: "number" },
        deposit_amount: { type: "number" },
        reduce_amount: { type: "number" },
        promo_code: { type: "text" },
        status: { type: "text" },
        referral_email: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        legacy_id: { type: "text" },
        number: { type: "text" }
    });

    var PgOrderItem = postgresDb.define('order_items', {
        order_id: { type: "number" },
        unit_price: { type: "number" },
        quantity: { type: "number" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        as_order_item_type: { type: "text" },
        as_order_item_id: { type: "number" },
        product_id: { type: "number" },
        legacy_id: { type: "text" }
    });

    var PgOrderSplitment = postgresDb.define('order_splitments', {
        order_id: { type: "number" },
        user_id: { type: "number" },
        email: { type: "text" },
        shared_price: { type: "number" },
        accepted: { type: "number" },
        sent_mail: { type: "boolean" },
        status: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        legacy_id: { type: "text" }
    });

    var PgOrderSplitmentProduct = postgresDb.define('order_splitments_products', {
        order_splitment_id: { type: "number" },
        product_id: { type: "number" }
    });

    var PgPayment = postgresDb.define('payments', {
        order_id: { type: "number" },
        order_splitment_id: { type: "number" },
        is_deposit: { type: "boolean" },
        is_full_payment: { type: "boolean" },
        amount: { type: "number" },
        as_payment_id: { type: "number" },
        as_payment_type: { type: "text" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        status: { type: "text" },
        user_id: { type: "number" }
    });

    var PgPaypalPayment = postgresDb.define('paypal_payments', {
        card_code: { type: "text" },
        card_holder_name: { type: "text" },
        card_month: { type: "text" },
        card_number: { type: "text" },
        card_type: { type: "text" },
        card_year: { type: "text" },
        payment_method: { type: "text" },
        purchase_billing_email: { type: "text" },
        purchase_billing_fullname: { type: "text" },
        purchase_billing_phone: { type: "text" },
        purchase_diff_billing: { type: "boolean" },
        purchase_email_sent_user: { type: "boolean" },
        purchase_payment_id: { type: "text" },
        purchase_sale_id: { type: "text" },
        purchase_paid_date: { type: "date" },
        purchase_refund_trx_id: { type: "text" },
        purchase_refund_state: { type: "text" },
        purchase_refund_date: { type: "date" },
        created_at: { type: "date" },
        updated_at: { type: "date" },
        legacy_order_splitment_id: { type: "text" }
    });

    var PgLegacyOrderItem = postgresDb.define('legacy_order_items', {
        created_at: { type: "date" },
        updated_at: { type: "date" }
    });

    function createPgUser(user) {
        var deferred = Q.defer();

        console.log('User ' + user.email);

        PgUser.create([{
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            admin: user.role_admin,
            active: true,
            phone: user.phone,
            legacy_id: user._id,
            has_password: false,
            created_at: user.created_at,
            updated_at: user.updated_at
        }], function (err, newUsers) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(newUsers[0]);
            }
        });
        return deferred.promise;
    }

    function createPgFbinfo(user, pgUser) {
        var deferred = Q.defer();

        PgFbinfo.create([{
            user_id: pgUser.id,
            uid: user.uid,
            nickname: user.fb_nickname,
            image: user.fb_image,
            link: user.fb_link,
            email: user.fb_email,
            name: user.fb_name,
            token: user.fb_token,
            token_expires: true,
            token_expires_at: user.fb_token_expires_at ? parseInt(user.fb_token_expires_at, 10) : null,
            created_at: user.created_at,
            updated_at: user.updated_at,
            first_name: null,
            last_name: null,
            timezone: user.fb_timezone ? parseInt(user.fb_timezone, 10) : null,
            birthday: user.fb_birthday,
            gender: user.fb_gender,
            location_id: user.fb_location_id,
            location_name: user.fb_location_name,
            hometown_id: user.fb_hometown_id,
            hometown_name: user.fb_hometown_name,
            locale: user.fb_locale
        }], function (err, newFbinfos) {
            var fbinfo;

            if (err) {
                deferred.reject(new Error(err));
            } else {
                fbinfo = newFbinfos[0];

                Q.fcall(function () {
                    if (user.fb_work_employer_id) {
                        return createPgFbinfoWork(user, fbinfo);
                    }
                    return null;
                }).then(function () {
                    if (user.fb_work_employer_id) {
                        return createPgFbinfoWork(user, fbinfo);
                    }
                    return null;
                }).then(function () {
                    if (user.fb_education_college_id) {
                        createCollegeFbinfoEducation(user, fbinfo);
                    }
                    return null;
                }).then(function () {
                    if (user.fb_education_high_school_id) {
                        createHighSchoolFbinfoEducation(user, fbinfo);
                    }
                    return null;
                }).then(function () {
                    deferred.resolve(fbinfo);
                }).fail(function (error) {
                    deferred.reject(new Error(error));
                });
            }
        });
        
        return deferred.promise;
    }

    function createPgFbinfoWork(user, pgFbinfo) {
        var deferred = Q.defer();

        PgFbinfoWork.create([{
            fbinfo_id: pgFbinfo.id,
            employer_id: user.fb_work_employer_id,
            employer_name: user.fb_work_employer_name,
            created_at: user.created_at,
            updated_at: user.updated_at,
            position_id: user.fb_work_position_id,
            position_name: user.fb_work_position_name,
            start_date: user.fb_work_start_date,
            end_date: user.fb_work_end_date,
            location_id: user.fb_work_location_id,
            location_name: user.fb_work_location_name
        }], function (err, newFbinfoWorks) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(newFbinfoWorks[0]);
            }
        });

        return deferred.promise;
    }

    function createCollegeFbinfoEducation(user, pgFbinfo) {
        var deferred = Q.defer();

        PgFbinfoEducation.create([{
            fbinfo_id: pgFbinfo.id,
            school_id: user.fb_education_college_id,
            school_name: user.fb_education_college_name,
            education_type: 'College',
            year_id: user.fb_education_college_year_id,
            year_name: user.fb_education_college_year_name,
            created_at: user.created_at,
            updated_at: user.updated_at
        }], function (err, newFbinfoEducations) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(newFbinfoEducations[0]);
            }
        });

        return deferred.promise;
    }

    function createHighSchoolFbinfoEducation(user, pgFbinfo) {
        var deferred = Q.defer();

        PgFbinfoEducation.create([{
            fbinfo_id: pgFbinfo.id,
            school_id: user.fb_education_high_school_id,
            school_name: user.fb_education_high_school_name,
            education_type: 'High School',
            year_id: user.fb_education_high_school_year_id,
            year_name: user.fb_education_high_school_year_name,
            created_at: user.created_at,
            updated_at: user.updated_at
        }], function (err, newFbinfoEducations) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(newFbinfoEducations[0]);
            }
        });

        return deferred.promise;
    }

    function findUsers() {
        var deferred = Q.defer(),
            stream = User.find().stream(),
            foundUsers = [];

        stream.on('data', function (user) {
            stream.pause();

            console.log('Checking User ' + user.email);
            OrderSplitment.where({ user_id: user._id }).limit(1).exec(function (err, splitments) {
                var hasSplitment;

                if (err) {
                    console.log(err);
                    stream.destroy(err);
                } else {
                    if (splitments.length > 0) {
                        foundUsers.push(user);
                    } else {
                        console.log('User ' + user.email + ' (Not Imported)');
                    }
                    stream.resume();
                }
            });
        }).on('error', function (err) {
            deferred.reject(new Error(err));
        }).on('close', function () {
            deferred.resolve(foundUsers);
        });

        return deferred.promise;
    }

    function migrateUsers(users) {
        var deferred = Q.defer();

        async.eachSeries(users,
            function (user, callback) {
                createPgUser(user)
                .then(function (newUser) {
                    return createPgFbinfo(user, newUser);
                }).then(function (newFbInfo) {
                    callback();
                }).fail(function (err) {
                    callback(err);
                });
            },
            function (err) {
                if (err) {
                    deferred.reject(new Error(err));
                } else {
                    deferred.resolve();
                }
            }
        );

        return deferred.promise;
    }

    function findEvent(id) {
        var deferred = Q.defer();

        Event.findOne({ _id: id }, function (err, event) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (!event) {
                deferred.reject('Cannot find event ' + id);
            } else {
                deferred.resolve(event);
            }
        });

        return deferred.promise;
    }

    function findEvents() {
        var deferred = Q.defer();

        Event.find(function (err, events) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                async.filterSeries(events, function (event, callback) {
                    Order.where({ event_id: event._id, status: { $ne: 'Open' } }).limit(1).exec(function (err, orders) {
                        var hasOrders;

                        if (err) {
                            deferred.reject(new Error(err));
                        } else {
                            hasOrders = orders.length > 0;
                            if (!hasOrders) {
                                console.log('Event ' + event._id + ' (Not Imported)');
                            }
                            callback(hasOrders);
                        }
                    });
                }, function (filteredEvents) {
                    deferred.resolve(filteredEvents);
                });
            }
        });

        return deferred.promise;
    }

    function findPgStateId(stateName) {
        var deferred = Q.defer();

        PgState.find({ or: [{ abbr: stateName }, { name: stateName }] }, 1, function (err, states) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(states.length ? states[0].id : null);
            }
        });

        return deferred.promise;
    }

    function createPgEvent(event, theUSId) {
        var deferred = Q.defer(),
            stateOrCountryName = event.event_state.trim();

        Q.fcall(function () {
            return findPgStateId(stateOrCountryName);
        }).then(function (stateId) {
            var subDeferred = Q.defer();

            console.log('Event ' + event._id);

            Q.fcall(function () {
                var countryDeferred;

                if (stateId) {
                    return theUSId;
                }

                countryDeferred = Q.defer();

                PgCountry.find({ name: stateOrCountryName }, 1, function (err, countries) {
                    if (err) {
                        countryDeferred.reject(new Error(err));
                    } else if (countries.length === 0) {
                        console.warn("============ Warning: Cannot find state or country " + stateOrCountryName);
                        countryDeferred.resolve(null);
                    } else {
                        countryDeferred.resolve(countries[0].id);
                    }
                });

                return countryDeferred.promise;
            }).then(function (countryId) {
                var creationDefferred = Q.defer();

                PgEvent.create([{
                    name: event.name,
                    slug: event._id,
                    description: event.event_description,
                    deadline: event.event_deadline,
                    start_date: event.event_start_date_data_type,
                    end_date: event.event_end_date_data_type,
                    address: event.event_address_1,
                    city: event.event_city,
                    zip: event.event_zip,
                    country_id: countryId,
                    latitude: event.event_latitude,
                    longitude: event.event_longitude,
                    image_1: event.event_image_1,
                    priority: event.event_priority,
                    visible: event.event_visible,
                    active: event.event_active,
                    created_at: event.created_at,
                    updated_at: event.updated_at,
                    state_id: stateId,
                    event_type: 'Legacy',
                    legacy_id: event._id,
                    start_time: event.event_start_time || null,
                    end_time: event.event_end_time || null
                }], function (err, newEvents) {
                    if (err) {
                        creationDefferred.reject(new Error(err));
                    } else {
                        creationDefferred.resolve(newEvents[0]);
                    }
                });

                return creationDefferred.promise;
            }).then(function (newEvent) {
                subDeferred.resolve(newEvent);
            }).fail(function (err) {
                subDeferred.reject(err);
            });

            return subDeferred.promise;
        }).then(function (newEvent) {
            deferred.resolve(newEvent);
        }).fail(function (err) {
            deferred.reject(err);
        });
        
        return deferred.promise;
    }

    function findUSCountryId() {
        var deferred = Q.defer();

        PgCountry.find({ name: 'United States' }, 1, function (err, countries) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (countries.length === 0) {
                deferred.reject(new Error('Cannot find United States country. Please seed initial data first.'));
            } else {
                deferred.resolve(countries[0].id);
            }
        });

        return deferred.promise;
    }

    function migrateEvents(events) {
        var deferred = Q.defer();

        Q.fcall(function () {
            return findUSCountryId();
        }).then(function (theUSId) {
            var subDeferred = Q.defer();

            async.eachSeries(events,
                function (event, callback) {
                    createPgEvent(event, theUSId)
                    .then(function (newEvent) {
                        callback();
                    }).fail(function (err) {
                        callback(err);
                    });
                },
                function (err) {
                    if (err) {
                        subDeferred.reject(new Error(err));
                    } else {
                        subDeferred.resolve();
                    }
                }
            );

            return subDeferred.promise;
        }).then(function () {
            deferred.resolve();
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function findProducts() {
        var deferred = Q.defer();

        Product.find(function (err, products) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                async.filterSeries(products, function (product, callback) {
                    OrderItem.findOne({ product_id: product._id, status: 'Ordered' }, function (err, orderItem) {
                        var found;

                        if (err) {
                            deferred.reject(new Error(err));
                        } else {
                            found = !!orderItem;
                            if (!found) {
                                console.log('Product ' + product.name + ' (Not Imported)');
                            }
                            callback(found);
                        }
                    });
                }, function (filteredProducts) {
                    deferred.resolve(filteredProducts);
                });
            }
        });

        return deferred.promise;
    }

    function createPgVendor(name, slug) {
        var now = new Date(),
            deferred = Q.defer();

        PgVendor.create([{
            name: name,
            slug: slug,
            created_at: now,
            updated_at: now
        }], function (err, vendors) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(vendors[0]);
            }
        });

        return deferred.promise;
    }

    function getLegacyVendorId() {
        var deferred = Q.defer();

        PgVendor.find({ name: 'Legacy' }, 1, function (err, vendors) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (vendors.length === 0) {
                deferred.reject(new Error('Cannot find Legacy vendor.'));
            } else {
                deferred.resolve(vendors[0].id);
            }
        });

        return deferred.promise;
    }

    function createPgProductImage(product, pgBaseProduct) {
        var deferred = Q.defer(),
            now = new Date();

        PgProductImage.create([{
            product_id: pgBaseProduct.id,
            url: product.product_image_1,
            is_visible: true,
            created_at: now,
            updated_at: now
        }], function (err, images) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(images[0]);
            }
        });

        return deferred.promise;
    }

    function createPgBaseProduct(product, as_product_id, as_product_type, vendorId) {
        var deferred = Q.defer();

        Q.fcall(function () {
            var subDeferred;

            if (!product.address) {
                return null;
            }

            subDeferred = Q.defer();

            Q.fcall(function () {
                return findPgStateId(product.state);
            }).then(function (stateId) {
                var outerDeferred = Q.defer();

                Q.fcall(function () {
                    return findUSCountryId();
                }).then(function (theUSId) {
                    var creationDeferred = Q.defer();

                    PgAddress.create([{
                        line_1: product.address,
                        city: product.city,
                        state_id: stateId,
                        country_id: theUSId
                    }], function (err, addresses) {
                        if (err) {
                            creationDeferred.reject(new Error(err));
                        } else {
                            creationDeferred.resolve(addresses[0].id);
                        }
                    });

                    return creationDeferred.promise;
                }).then(function (addressId) {
                    outerDeferred.resolve(addressId);
                }).fail(function (err) {
                    outerDeferred.reject(err);
                });

                return outerDeferred.promise;
            }).then(function (addressId) {
                subDeferred.resolve(addressId);
            }).fail(function (err) {
                subDeferred.reject(err);
            });

            return subDeferred.promise;
        }).then(function (addressId) {
            var subDeferred = Q.defer();

            PgBaseProduct.create([{
                name: product.name,
                slug: product._id,
                description: product.long_description,
                created_at: product.created_at,
                updated_at: product.updated_at,
                vendor_id: vendorId,
                as_product_id: as_product_id,
                as_product_type: as_product_type,
                address_id: addressId,
                active: product.active,
                visible: product.product_visible,
                master_price: product.price
            }], function (err, products) {
                if (err) {
                    subDeferred.reject(new Error(err));
                } else {
                    subDeferred.resolve(products[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (newBaseProduct) {
            Q.fcall(function () {
                if (!product.product_image_1) {
                    return null;
                }
                return createPgProductImage(product, newBaseProduct);
            }).then(function () {
                deferred.resolve(newBaseProduct);
            }).fail(function (err) {
                deferred.reject(err);
            });            
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function createPgLegacyProduct(product, eventId, vendorId) {
        var deferred = Q.defer();

        Q.fcall(function () {
            var subDeferred = Q.defer();

            console.log('Product ' + product.name);

            PgLegacyProduct.create([{
                product_type: product.product_type,
                company_name: product.company_name || null,
                product_embed_html: product.product_embed_html,
                embed_html: product.embed_html || null,
                quantity: product.quantity,
                instock: product.instock,
                event_id: eventId,
                legacy_id: product._id,
                created_at: product.created_at,
                updated_at: product.updated_at
            }], function (err, legacyProducts) {
                if (err) {
                    subDeferred.reject(new Error(err));
                } else {
                    subDeferred.resolve(legacyProducts[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (legacyProduct) {
            return createPgBaseProduct(product, legacyProduct.id, 'LegacyProduct', vendorId);
        }).then(function () {
            deferred.resolve();
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function migrateProducts(products) {
        var deferred = Q.defer();

        Q.fcall(function () {
            return createPgVendor('Legacy', 'legacy');
        }).then(function (vendor) {
            var subDeferred = Q.defer();

            async.eachSeries(products,
                function (product, callback) {
                    findPgEventIdFromLegacyEventId(product.event_id)
                    .then(function (pgEventId) {
                        return createPgLegacyProduct(product, pgEventId, vendor.id);
                    }).then(function () {
                        callback();
                    }).fail(function (err) {
                        callback(err);
                    });
                },
                function (err) {
                    if (err) {
                        subDeferred.reject(new Error(err));
                    } else {
                        subDeferred.resolve();
                    }
                }
            );

            return subDeferred.promise;
        }).then(function () {
            deferred.resolve();
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function findOrders() {
        var deferred = Q.defer();

        Order.find({ status: { $ne: 'Open' } }, function (err, orders) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(orders);
            }
        });

        return deferred.promise;
    }

    function findPgEventIdFromLegacyEventId(legacyEventId) {
        var deferred = Q.defer();

        PgEvent.find({ legacy_id: legacyEventId }, 1, function (err, events) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (events.length === 0) {
                deferred.reject(new Error('Cannot find any event with legacy id ' + legacyEventId));
            } else {
                deferred.resolve(events[0].id);
            }
        });

        return deferred.promise;
    }

    function findPgUserIdFromLegacyUserId(legacyUserId) {
        var deferred = Q.defer();

        PgUser.find({ legacy_id: legacyUserId }, 1, function (err, users) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (users.length === 0) {
                deferred.reject(new Error('Cannot find any user with legacy id ' + legacyUserId));
            } else {
                deferred.resolve(users[0].id);
            }
        });

        return deferred.promise;
    }

    function findPgBaseProductIdFromLegacyProductId(legacyProductId, noRaisingError) {
        var deferred = Q.defer();

        PgLegacyProduct.find({ legacy_id: legacyProductId }, 1, function (err, pgLegacyProducts) {
            if (err) {
                deferred.reject(new Error(err));
            } else if (pgLegacyProducts.length === 0) {
                if (noRaisingError) {
                    deferred.resolve(null);
                } else {
                    deferred.reject(new Error('Cannot find any legacy product with legacy id ' + legacyProductId));
                }
            } else {
                PgBaseProduct.find({ as_product_id: pgLegacyProducts[0].id, as_product_type: 'LegacyProduct' }, 1, function (err, baseProducts) {
                    if (err) {
                        deferred.reject(new Error(err));
                    } else if (baseProducts.length === 0) {
                        if (noRaisingError) {
                            deferred.resolve(null);
                        } else {
                            deferred.reject(new Error('Cannot find any base product of type LagacyProduct having product id ' + pgLegacyProducts[0].id));
                        }
                    } else {
                        deferred.resolve(baseProducts[0].id);
                    }
                });
            }
        });

        return deferred.promise;
    }

    function convertArrayToLiteral(array) {
        var tmpArray = [],
            i;

        if (!array) {
            return '{}';
        }

        for (i = 0; i < array.length; i++) {
            if (typeof array[i] === 'string') {
                tmpArray[i] = '"' + array[i] + '"';
            } else {
                tmpArray[i] = array[i];
            }
        }

        return '{' + tmpArray.join(', ') + '}';
    }

    function createPgOrder(order) {
        var deferred = Q.defer();

        Q.all([
            findPgEventIdFromLegacyEventId(order.event_id),
            findPgUserIdFromLegacyUserId(order.buyer_id)
        ]).spread(function (eventId, buyerId) {
            var subDeferred = Q.defer();

            console.log('Order ' + order._id);

            PgOrder.create([{
                event_id: eventId,
                buyer_id: buyerId,
                total_price: order.total_price,
                deposit_amount: 0,
                reduce_amount: order.reduce_amount,
                promo_code: order.promote_code,
                status: order.status === 'Complete' ? 'Completed' : order.status,
                referral_email: order.referral_email || null,
                created_at: order.created_at,
                updated_at: order.updated_at,
                legacy_id: order._id,
                number: order._id
            }], function (err, newOrders) {
                if (err) {
                    subDeferred.reject(new Error(err));
                } else {
                    subDeferred.resolve(newOrders[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (newOrder) {
            deferred.resolve(newOrder);
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function findOrderItems(order) {
        var deferred = Q.defer();

        OrderItem.find({ order_id: order._id, status: 'Ordered' }, function (err, orderItems) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(orderItems);
            }
        });

        return deferred.promise;
    }

    function findOrderSplitments(order) {
        var deferred = Q.defer();

        OrderSplitment.find({ order_id: order._id }, function (err, orderSplitments) {
            if (err) {
                deferred.reject(new Error(err));
            } else {
                deferred.resolve(orderSplitments);
            }
        });

        return deferred.promise;
    }

    function createBasePgOrderItem(orderIem, pgOrderId, as_order_item_type, as_order_item_id) {
        var deferred = Q.defer();

        findPgBaseProductIdFromLegacyProductId(orderIem.product_id)
        .then(function (baseProductId) {
            var subDeferred = Q.defer();

            PgOrderItem.create([{
                order_id: pgOrderId,
                unit_price: orderIem.unit_price,
                quantity: orderIem.quantity,
                created_at: orderIem.created_at,
                updated_at: orderIem.updated_at,
                as_order_item_type: as_order_item_type,
                as_order_item_id: as_order_item_id,
                product_id: baseProductId,
                legacy_id: orderIem._id
            }], function (err, newOrderItems) {
                if (err) {
                    subDeferred.reject(err);
                } else {
                    subDeferred.resolve(newOrderItems[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (newOrderItem) {
            deferred.resolve(newOrderItem);
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function createPgOrderSplitment(orderSplitment, pgOrderId) {
        var deferred = Q.defer();

        Q.fcall(function () {
            if (orderSplitment.user_id) {
                return findPgUserIdFromLegacyUserId(orderSplitment.user_id);
            }
            return null;
        }).then(function (pgUserId) {
            var subDeferred = Q.defer();

            console.log('Order splitment ' + orderSplitment._id);

            PgOrderSplitment.create([{
                order_id: pgOrderId,
                user_id: pgUserId,
                email: orderSplitment.email,
                shared_price: orderSplitment.shared_price,
                accepted: orderSplitment.accepted,
                sent_mail: orderSplitment.sent_mail,
                status: orderSplitment.status,
                created_at: orderSplitment.created_at,
                updated_at: orderSplitment.updated_at,
                legacy_id: orderSplitment._id
            }], function (err, splitments) {
                if (err) {
                    subDeferred.reject(err);
                } else {
                    subDeferred.resolve(splitments[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (newSplitment) {
            if (orderSplitment.product_list && orderSplitment.product_list.length) {
                async.eachSeries(orderSplitment.product_list, function (legacyProductId, callback) {
                    findPgBaseProductIdFromLegacyProductId(legacyProductId, true).then(function (pgProductId) {
                        if (!pgProductId) {
                            callback();
                            return;
                        }

                        PgOrderSplitmentProduct.create([{
                            order_plitment_id: newSplitment.id,
                            product_id: pgProductId
                        }], function (err, newOrderSplitmentProducts) {
                            callback(err);
                        });
                    }).fail(function (err) {
                        callback(err);
                    });
                }, function (err) {
                    if (err) {
                        deferred.reject(err);
                    } else {
                        deferred.resolve(newSplitment);
                    }
                });
            } else {
                deferred.resolve(newSplitment);
            }
        }).fail(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    }

    function createPgPayment(orderSplitment, pgOrderId, pgOrderSplitmentId, pgUserId, is_full_payment, as_payment_id, as_payment_type) {
        var deferred = Q.defer();

        PgPayment.create([{
            order_id: pgOrderId,
            order_splitment_id: pgOrderSplitmentId,
            is_deposit: false,
            is_full_payment: is_full_payment,
            amount: orderSplitment.shared_price,
            as_payment_id: as_payment_id,
            as_payment_type: as_payment_type,
            created_at: orderSplitment.updated_at, // in old system, payment has not been made at the time of creating splitment
            updated_at: orderSplitment.updated_at,
            status: orderSplitment.status,
            user_id: pgUserId
        }], function(err, payments) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(payments[0]);
            }
        });

        return deferred.promise;
    }

    function createPgPaypalPayment(orderSplitment) {
        var deferred = Q.defer();

        PgPaypalPayment.create([{
            card_code: orderSplitment.card_code,
            card_holder_name: orderSplitment.card_holder_name,
            card_month: orderSplitment.card_month,
            card_number: orderSplitment.card_number,
            card_type: orderSplitment.card_type,
            card_year: orderSplitment.card_year,
            payment_method: orderSplitment.payment_method,
            purchase_billing_email: orderSplitment.purchase_billing_email,
            purchase_billing_fullname: orderSplitment.purchase_billing_fullname,
            purchase_billing_phone: orderSplitment.purchase_billing_phone || null,
            purchase_diff_billing: orderSplitment.purchase_diff_billing,
            purchase_email_sent_user: orderSplitment.purchase_email_sent_user,
            purchase_payment_id: orderSplitment.purchase_payment_id,
            purchase_sale_id: orderSplitment.purchase_sale_id,
            purchase_paid_date: orderSplitment.purchase_paid_date,
            purchase_refund_trx_id: orderSplitment.purchase_refund_trx_id,
            purchase_refund_state: orderSplitment.purchase_refund_state,
            purchase_refund_date: orderSplitment.purchase_refund_date,
            created_at: orderSplitment.created_at,
            updated_at: orderSplitment.updated_at,
            legacy_order_splitment_id: orderSplitment._id
        }], function (err, payments) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(payments[0]);
            }
        });

        return deferred.promise;
    }

    function createPgLegacyOrderItem(orderItem, pgOrderId, event) {
        var deferred = Q.defer();

        Q.fcall(function () {
            var subDeferred = Q.defer();

            PgLegacyOrderItem.create([{
                created_at: orderItem.created_at,
                updated_at: orderItem.updated_at
            }], function (err, legacyOrderItems) {
                if (err) {
                    subDeferred.reject(err);
                } else {
                    subDeferred.resolve(legacyOrderItems[0]);
                }
            });

            return subDeferred.promise;
        }).then(function (legacyOrderItem) {
            return createBasePgOrderItem(orderItem, pgOrderId, 'LegacyOrderItem', legacyOrderItem.id);
        }).then(function () {
            deferred.resolve();
        }).fail(function (err) {
            deferred.reject(new Error(err));
        });

        return deferred.promise;
    }

    function getEventOfOrder(order) {
        var subDeferred = Q.defer();

        Event.find({ _id: order.event_id }, function (err, events) {
            if (err) {
                subDeferred.reject(err);
            } else if (events.length === 0) {
                subDeferred.reject("Cannot find any event having id " + order.event_id);
            } else {
                subDeferred.resolve(events[0]);
            }
        });

        return subDeferred.promise;
    }

    function migrateOrders(orders) {
        var deferred = Q.defer();

        async.eachSeries(orders,
            function (order, callback) {
                Q.all([
                    createPgOrder(order),
                    getEventOfOrder(order),
                    findOrderItems(order)
                ]).spread(function (pgOrder, event, orderItems) {
                    var subDeferred = Q.defer();

                    Q.fcall(function () {
                        return findOrderSplitments(order).then(function (orderSplitments) {
                            var splitmentDeferred = Q.defer(),
                                isFullPayment = (orderSplitments.length === 1) && (pgOrder.total_price === orderSplitments[0].shared_price);

                            async.eachSeries(orderSplitments, function (orderSplitment, callback) {
                                Q.fcall(function () {
                                    if (isFullPayment) {
                                        return null;
                                    }
                                    return createPgOrderSplitment(orderSplitment, pgOrder.id);
                                }).then(function (pgOrderSplitment) {
                                    // Splitment has not been paid yet
                                    if (orderSplitment.status === 'Open') {
                                        return null;
                                    }
                                    return findPgUserIdFromLegacyUserId(orderSplitment.user_id).
                                        then(function (pgUserId) {
                                            return createPgPaypalPayment(orderSplitment)
                                            .then(function (pgPaypalPayment) {
                                                return createPgPayment(orderSplitment, pgOrder.id, pgOrderSplitment ? pgOrderSplitment.id : null,
                                                    pgUserId, isFullPayment, pgPaypalPayment.id, 'PaypalPayment');
                                            });
                                        });
                                }).then(function () {
                                    callback();
                                }).fail(function (err) {
                                    callback(err);
                                });
                            }, function (err) {
                                if (err) {
                                    splitmentDeferred.reject(err);
                                } else {
                                    splitmentDeferred.resolve();
                                }
                            });

                            return splitmentDeferred.promise;
                        });
                    }).then(function () {
                        var orderItemsDeferred = Q.defer();

                        async.eachSeries(orderItems, function (orderItem, callback) {
                            createPgLegacyOrderItem(orderItem, pgOrder.id, event)
                            .then(function () {
                                callback();
                            }).fail(function (err) {
                                callback(err);
                            });
                        }, function (err) {
                            if (err) {
                                orderItemsDeferred.reject(err);
                            } else {
                                orderItemsDeferred.resolve();
                            }
                        });

                        return orderItemsDeferred.promise;
                    }).then(function () {
                        subDeferred.resolve();
                    }).fail(function (err) {
                        subDeferred.reject(err);
                    });

                    return subDeferred.promise;
                }).then(function() {
                    callback();
                }).fail(function (err) {
                    callback(err);
                });
            },
            function (err) {
                if (err) {
                    deferred.reject(new Error(err));
                } else {
                    deferred.resolve();
                }
            }
        );

        return deferred.promise;
    }

    findUsers().then(migrateUsers).then(findEvents).then(migrateEvents)
    .then(findProducts).then(migrateProducts)
    .then(findOrders).then(migrateOrders)
    .then(function () {
        console.log('DONE!');
    }).fail(function (err) {
        console.error(err);
    });
});