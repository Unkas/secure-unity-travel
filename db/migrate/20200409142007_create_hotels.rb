class CreateHotels < ActiveRecord::Migration
  def up
    create_table :tags do |t|
      t.string :name
      t.integer :color_scheme
      t.timestamps
    end
    add_index :tags, :name, :unique => true

    create_table :hotels do |t|
      t.string :name
      t.float :price
      t.float :rating
      t.string :address
      t.string :image_address
      t.timestamps
    end
    add_index :hotels, :name
    add_index :hotels, :price
    add_index :hotels, :rating

    create_table :hotel_tags do |t|
      t.references :hotel
      t.references :tag
      t.timestamps
    end
  end

  def down
    drop_table :hotel_tags
    drop_table :hotels
    drop_table :tags
  end
end
