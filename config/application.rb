require File.expand_path('boot', __dir__)

require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'sprockets/railtie'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module JusCollege
  class Application < Rails::Application
    config.assets.precompile += %w[*.png *.jpg *.jpeg *.gif non-admin.js admin.js non-admin.css admin.css]

    # Filepicker.io
    config.filepicker_rails.api_key = ENV['FILEPICKER_API_KEY'] || 'ApV72tjTRW6y3Q1oKqbTdz'

    # Avoid deprecated message
    config.i18n.enforce_available_locales = false
    config.time_zone = 'America/Los_Angeles'

    # SASS-RAILS
    # config.sass.preferred_syntax = :sass
    # config.sass.line_comments = false
    # config.sass.cache = false
  end
end
