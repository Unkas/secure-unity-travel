# Site map configuration
SitemapGenerator::Sitemap.default_host = MAIN_SERVER_URL
SitemapGenerator::Sitemap.sitemaps_host = SITE_MAP_ADDRESS
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = "#{UPLOAD_FOLDER}/sitemaps/"
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new

SitemapGenerator::Sitemap.create do
  add '/event-list', :changefreq => 'weekly'

  # Add events links
  Event.future_events.where{(show_in_sitemap == true)}.order(:priority, :start_date).find_each do |event|
    add event_path(event), :lastmod => event.updated_at, :changefreq => 'weekly'
  end

  # Add static page links
  StaticPage.where{(show_in_sitemap == true) & (active == true)}.find_each do |page|
    add "/#{page.slug}", :priority => 0.4, :lastmod => page.updated_at, :changefreq => 'monthly'
  end

end