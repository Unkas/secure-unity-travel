AWS.config(
    access_key_id: AMAZON_S3_CONFIG[:ACCESS_KEY_ID],
    secret_access_key: AMAZON_S3_CONFIG[:ACCESS_KEY_SECRET]
)