require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../config/environment', __dir__)

# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this point!
require 'capybara/rails'

require 'support/factory_girl'

require 'database_cleaner/active_record'

require 'shoulda/matchers'

DatabaseCleaner.strategy = :truncation

# OmniAuth.config.test_mode = true

omniauth_hash = {
  uid: '123456',
  provider: 'facebook',
  info: {
    nickname: 'Facebook nickname',
    email: 'abc@yahoo.com',
    image: '/images/no_image.png',
    name: 'My name'
  },
  extra: {
    raw_info: { link: 'http:\facebook.com\abc' }
  },
  credentials: {
    token: 'token234facebook',
    expires_at: '2019-01-31',
    expires: ''
  }
}
# OmniAuth.config.add_mock(:facebook, omniauth_hash)


SimpleCov.start do
  add_filter "/vendor/"
end

RSpec.configure do |config|
  config.include Rails.application.routes.url_helpers
  Rails.application.routes.default_url_options[:host] = 'localhost'
  Rails.application.routes.default_url_options[:port] = '3000'

  config.use_transactional_fixtures = false

  config.before(:suite) do
    if config.use_transactional_fixtures?
      raise(<<-MSG)
        Delete line `config.use_transactional_fixtures = true` from rails_helper.rb
        (or set it to false) to prevent uncommitted transactions being used in
        JavaScript-dependent specs.

        During testing, the app-under-test that the browser driver connects to
        uses a different database connection to the database connection used by
        the spec. The app's database connection would not be able to access
        uncommitted transaction data setup over the spec's database connection.
      MSG
    end
    DatabaseCleaner.clean_with(:truncation)

    Rails.application.load_seed # loading seeds
  end

  config.before do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, :truncate_before_running) do |_example|
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each, type: :feature) do
    # :rack_test driver's Rack app under test shares database connection
    # with the specs, so continue to use transaction strategy for speed.
    driver_shares_db_connection_with_specs = Capybara.current_driver == :rack_test

    unless driver_shares_db_connection_with_specs
      # Driver is probably for an external browser with an app
      # under test that does *not* share a database connection with the
      # specs, so use truncation strategy.
      DatabaseCleaner.strategy = :truncation
    end
  end

  config.before do
    DatabaseCleaner.start
  end

  config.append_after do
    DatabaseCleaner.clean
  end

  config.infer_base_class_for_anonymous_controllers = false

  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
end
