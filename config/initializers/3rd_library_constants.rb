AFFIRM_SUPPORTED = ENV['AFFIRM_PUBLIC_KEY'].present?

AFFIRM_CONFIG = {
  PUBLIC_KEY: ENV['AFFIRM_PUBLIC_KEY'],
  PRIVATE_KEY: ENV['AFFIRM_PRIVATE_KEY'],
  FINANCIAL_PRODUCT_KEY: ENV['AFFIRM_FINANCIAL_PRODUCT_KEY'],
  APP_URL: ENV['AFFIRM_APP_URL'],
  SCRIPT_URL: ENV['AFFIRM_SCRIPT_URL'],
  MAX_PAYMENT_AMOUNT: 17_500,
  MIN_APR: ENV['AFFIRM_MIN_APR'].present? ? ENV['AFFIRM_MIN_APR'] : 0.10
}.freeze

# Google API
GOOGLE_CONFIG = {
  API_KEY: ENV['GOOGLE_API_KEY'] || 'AIzaSyDo6kj2FfcA0KY_e_ztD6AoKK740cbTTBk',
  GOOGLE_ID: ENV['GOOGLE_ID'] || 'UA-57961761-1',
  APP_ID: ENV['GOOGLE_APP_ID'] || '786410526239-i56290h6ig75vshk0l60v68195qp26n7.apps.googleusercontent.com',
  APP_SECRET: ENV['GOOGLE_APP_SECRET'] || 'tTs0hHlsw760Dbjb_uAVzxHn',
  PERMISSION_SCOPE: ENV['GOOGLE_PERMISSION_SCOPE'] || 'email,profile'
}.freeze

# Slack
SLACK_CONFIG = {
  WEB_HOOK_URL: ENV['SLACK_HOOK_URL'] || 'https://hooks.slack.com/services/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'
}.freeze

# ArrowPass
ARROW_PASS_CONFIG = {
  HEADER_KEY: ENV['ARROW_PASS_HEADER_KEY'] || 'APS-CLIENT',
  HEADER_VALUE: ENV['ARROW_PASS_HEADER_VALUE'] || 'TOBE_DETERMINED_UNITY',
  API_URL: ENV['ARROW_PASS_API_URL'] || 'TOBE_DETERMINED_UNITY',
  MAX_DAY_TOTAL: ENV['ARROW_PASS_MAX_DAY_TOTAL'] || 7, # days
  TIME_RANGE: ENV['ARROW_PASS_TIME_RANGE'] || 15 # minutes
}.freeze

# Facebook
FACEBOOK_CONFIG = {
  APP_ID: ENV['FACEBOOK_APP_ID'] || '843043769102731',
  APP_SECRET: ENV['FACEBOOK_APP_SECRET'] || '0dffef9bfabbd26fba93779483cd31aa',
  PERMISSION_SCOPE: ENV['FACEBOOK_PERMISSION_SCOPE'] || 'email,public_profile',
  PIXEL_ID: ENV['FACEBOOK_PIXEL_ID'] || '1407439252892584'
}.freeze

# Twitter
TWITTER_CONFIG = {
  APP_ID: ENV['TWITTER_APP_ID'] || '8Uo1YZDKsFIe5t4ZqVNL1kh8m',
  APP_SECRET: ENV['TWITTER_APP_SECRET'] || 'URVJJFKTzLkf8hyoVkgiFlaZN2U6PRyEGrn9nAFoVwIm0IRi5N'
}.freeze

# ReCaptcha
RECAPTCHA_CONFIG = {
  ENABLED: ENV['RECAPTCHA_ENABLED'].present? && ENV['RECAPTCHA_ENABLED'] == 'true' ? true : false,
  PUBLIC_KEY: ENV['RECAPTCHA_PUBLIC_KEY'],
  PRIVATE_KEY: ENV['RECAPTCHA_PRIVATE_KEY']
}.freeze

# Amazon S3
AMAZON_S3_CONFIG = {
  ACCESS_KEY_ID: ENV['AMAZON_S3_ACCESS_KEY_ID'] || 'AKIAICMBGWJU67VJIKQQ',
  ACCESS_KEY_SECRET: ENV['AMAZON_S3_ACCESS_KEY_SECRET'] || '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',
  BUCKET_NAME: ENV['AMAZON_S3_BUCKET_NAME'] || 'unity-website'
}.freeze

# SendGrid
SMS_ENABLED = ENV['SMS_ENABLED'].present? && ENV['SMS_ENABLED'] == 'true' ? true : false

SEND_GRID_CONFIG = {
  USER_NAME: ENV['SEND_GRID_USER_NAME'] || 'unitytravel',
  PASSWORD: ENV['SEND_GRID_MAIL_PASSWORD'] || 'Unity2012!',
  DOMAIN: ENV['SEND_GRID_DOMAIN'] || 'unity.travel'
}.freeze

# Stripe
PAYMENT_DETAIL_LINK = ENV['STRIPE_SECRET_KEY'].blank? ? 'https://dashboard.stripe.com/test/payments/' : 'https://dashboard.stripe.com/payments/'

STRIPE_CONFIG = {
  PUBLISHABLE_KEY: ENV['STRIPE_PUBLISHABLE_KEY'] || 'pk_test_V5F3cRXKiUl4YJIs424g4ZAA00wtQF10IE',
  SECRET_KEY: ENV['STRIPE_SECRET_KEY'] || 'rk_test_kJCFIbK9VL2XEDiTaaMO5CTo007N6qOffs'
}.freeze

# Bitly
BITLY_CONFIG = {
  API_VERSION: ENV['BITLY_API_VERSION'] || 3,
  ACCESS_TOKEN: ENV['BITLY_ACCESS_TOKEN'] || 'R_97c14871326542109bad50dd0d630524'
}.freeze

# Twilio
TWILIO = {
  ACCOUNT_SID: ENV['TWILIO_ACCOUNT_SID'] || 'AC84357b3cf8cfbce5eea09a68a7e48c1e',
  AUTH_TOKEN: ENV['TWILIO_AUTH_TOKEN'] || 'd00a8f51e5047394958c173a9dff14ba',
  FROM_NUMBER: ENV['TWILIO_FROM_NUMBER'] || '+15005550006',
  EXTERNAL_HACK_BASE_URL: ENV['TWILIO_EXTERNAL_HACK_BASE_URL'],
  EXTERNAL_HACK_URL_PATH: ENV['TWILIO_EXTERNAL_HACK_URL_PATH'] || '/send_message',
  EXTERNAL_HACK_PASSWORD: ENV['TWILIO_EXTERNAL_HACK_PASSWORD']
}.freeze

REFUND_PROTECT_CONFIG = {
  VENDOR_ID: ENV['REFUND_PROTECT_VENDOR_ID'] || 'UNITYT-D9D6-DF44',
  API_KEY: ENV['REFUND_PROTECT_API_KEY'] || '56FB1FFA-451D-40E8-B263-E206623CFA5A',
  URL: ENV['REFUND_PROTECT_URL'] || 'https://api-uat.protect-platform.com/api/v1/refundprotect/salesoffering'
}.freeze

EVENT_PROTECT_CONFIG = {
  VENDOR_ID: ENV['EVENT_PROTECT_VENDOR_ID'] || ENV['REFUND_PROTECT_VENDOR_ID'] || 'UNITYT-D9D6-DF44',
  API_KEY: ENV['EVENT_PROTECT_API_KEY'] || ENV['REFUND_PROTECT_API_KEY'] || '56FB1FFA-451D-40E8-B263-E206623CFA5A',
  URL: ENV['REFUND_PROTECT_URL'] || 'https://api-uat.protect-platform.com/transactionapi/v1/transaction'
}.freeze
