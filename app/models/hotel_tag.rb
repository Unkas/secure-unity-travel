class HotelTag < ActiveRecord::Base

  belongs_to :tag
  belongs_to :hotel

  validates_presence_of :tag_id
  validates_presence_of :hotel_id
end
