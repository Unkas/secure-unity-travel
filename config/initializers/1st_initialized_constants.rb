# ----------------------------------------------------------------------------------------------
# Not depend on website and environment
# ----------------------------------------------------------------------------------------------
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
DASHBOARD = { :DATE_TYPES => { :GENERAL => 0, :EXACT => 1} }
LOAD_MORE_PAGE_SIZE = 25
MIN_PAGE_SIZE = 10
MIN_PAGE_SIZE_FOR_ALL = 50
MINIMUM_NOTIFICATION_ITEMS = 5
MAX_INSTOCK = 9999
MAX_TOTAL_ROOMMATES = 100
FILE_SIZE_LIMIT = 1000000 # BYTE (1MB)
MAX_ROOM_NIGHTS = 10
MAXIMUM_SEARCH_RESULTS_CSB_ROOM = 10
MAXIMUM_TIME_CAN_REMIND_AGAIN = 3

MINIMUM_PAYMENT_AMOUNT = 0.5
MINIMUM_PARTIAL_REFUND_PAYMENT_AMOUNT = 0.01
MAX_SPLIT_MEMBERS = 50
MAXIMUM_SEARCH_RESULTS_SOUVENIR = 5

# ----------------------------------------------------------------------------------------------
# Only depend on environment
# ----------------------------------------------------------------------------------------------
IS_PRODUCTION = ENV['RAILS_MAIL_ENV'].present? && ENV['RAILS_MAIL_ENV'] == 'production'
UPLOAD_FOLDER = IS_PRODUCTION ? 'production' : 'development'

DEFAULT_HOME_PAGE = ENV['DEFAULT_HOME_PAGE'] # only set this if we want to overide default value
MAIN_SERVER_HOST = ENV['MAIN_SERVER_HOST'] # only set this if we want to overide default value

INQUIRY_URL = ENV['INQUIRY_URL'] || '/inquiries/new'
AFFILIATE_REDIRECT_URL = ENV['AFFILIATE_REDIRECT_URL'] || '/reps'

EVENT_SOLD_OUT_EPSILON = ENV['EVENT_SOLD_OUT_EPSILON'] || 10
PRODUCT_REVIEW_AVAILABLE_DAYS = ENV['PRODUCT_REVIEW_AVAILABLE_DAYS'] || 45
NUMBER_OF_DAYS_AVAILABLE_FOR_CHECKIN = IS_PRODUCTION ? 7 : 365
CHECKIN_ACCESS_CODE = ENV['CHECKIN_ACCESS_CODE'].present? ? ENV['CHECKIN_ACCESS_CODE'] : '123456'
SHOW_CSB_ROOMING_LIST = ENV['SHOW_CSB_ROOMING_LIST'].present? && ENV['SHOW_CSB_ROOMING_LIST'] == 'true' ? true : false

GEO_LOCATION_ENABLED = ENV['GEOLOCATION_REPORT_ENABLED'].present? && ENV['GEOLOCATION_REPORT_ENABLED'] == 'true' ? true : false
REPORT_ROW_CONFIGURED_NUMBER = (ENV['REPORT_ROW_CONFIGURED_NUMBER'].present? && ENV['REPORT_ROW_CONFIGURED_NUMBER'].to_i >= 0) ? ENV['REPORT_ROW_CONFIGURED_NUMBER'].to_i : 200
SCHEDULE_MAX_SELECTED_RECIPIENTS = ENV['SCHEDULE_MAX_SELECTED_RECIPIENTS'] || 300
REMIND_SCHEDULE_DAYS = ENV['REMIND_SCHEDULE_DAYS'] || 2
REMIND_SCHEDULE_DEPOSIT_AMOUNT = ENV['REMIND_SCHEDULE_DEPOSIT_AMOUNT'] || 100
DELAY_SEND_CONFIRMATION_AUTO_CHARGE_HOURS = ENV['SEND_CONFIRMATION_AUTO_CHARGE_HOURS'] || 5
WEEKLY_REDEEM_REPORT_DAY = ENV['WEEKLY_REDEEM_REPORT_DAY'] || 'wednesday'
WEBSITE_KEY = ENV['WEBSITE_KEY'] || 'unity'

COMPANY_NAME = 'Unity Travel'
COMPANY_OFFICIAL_NAME = 'Unity Travel, LLC'
CONTACT_PHONE = '702-521-5321'
CONTACT_ADDRESS = '7373 Fleeting Joys Ave.'
CONTACT_CITY = 'Las Vegas'
CONTACT_STATE = '89113'
ENCRYPT_KEY = 'unity'
MAIN_SERVER_URL = MAIN_SERVER_HOST.blank? ? 'https://secure.unity.travel/event-list' : "https://#{MAIN_SERVER_HOST}"
SITE_MAP_ADDRESS = ENV['SITE_MAP_ADDRESS'] || 'https://unity-sitemaps.s3.amazonaws.com/',


# Default emails settings
DEV_TEAM_EMAIL = ENV['SUPER_ADMINS'] || %w(partounian@gmail.com josh@unity.travel patrick@unity.travel erika@unity.travel carlos@unity.travel),
TEST_EMAIL = 'patrick@unity.travel',

EMAIL_ADDRESS = {
    :NO_REPLY               => IS_PRODUCTION ? 'info@unity.travel' : TEST_EMAIL,
    :CONTACT                => IS_PRODUCTION ? 'info@unity.travel' : TEST_EMAIL,
    :CC_NOTIFY              => IS_PRODUCTION ? 'info@unity.travel' : TEST_EMAIL,
    :INQUIRY                => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :ACCOUNTANT_TEAM        => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :CTEAM                  => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :SALE_TEAM              => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :SALE_TRIAGE_TEAM       => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :INTERNAL_TEAM          => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :PASSPORT_TEAM          => IS_PRODUCTION ? 'reservations@unity.travel' : TEST_EMAIL,
    :DEV_REPORT_TEAM        => 'patrick@unity.travel',
}

# Open APIs
PUBLIC_API_CONFIG = {
    'KEY'         => 'Unity-HEADER',
    'ARROW_PASS'  => ENV['PUBLIC_API_ARROW_PASS_HEADER_VALUE'] || '123456@X'
}

# Social links
SOCIAL_LINK = {
    :FACEBOOK   => 'https://www.facebook.com/www.Unity.Travel/',
    :INSTAGRAM  => 'https://www.instagram.com/unitytravel',
    :TWITTER    => 'https://twitter.com/unitytravel',
    :SNAPCHAT   => '#'
}

# Static links
STATIC_LINK = {
    :HOW_TO_REDEEM    => '#'
}

# ----------------------------------------------------------------------------------------------
# Themes and colors
# ----------------------------------------------------------------------------------------------
VAR_COLORS = {
    :VAR_COLOR_1 => ENV['VAR_COLOR_1'] || '#1d4f83'
}

# ----------------------------------------------------------------------------------------------
# School vs Group
# ----------------------------------------------------------------------------------------------
SCHOOL_GROUP_TEXT = 'Group'

# ----------------------------------------------------------------------------------------------
# Facebook pixel tracking
# ----------------------------------------------------------------------------------------------
SOCIAL_TRACK = {
    :PARAM      => 'source',
    :FB_VALUE   => 'fb-ads',
    :FB_COOKIE  => 'social_source_fb_tracked',
    :FB_EXTRA_COOKIE  => 'extra_social_source_fb_tracked'
}
