require 'rails_helper'

RSpec.describe Tag, type: :model do
  it { should validate_presence_of :name }

  describe 'dependencies' do
    it { should have_many(:hotel_tags).dependent(:destroy) }
  end
end
