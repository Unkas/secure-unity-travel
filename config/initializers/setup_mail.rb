# SendGrid setup
ActionMailer::Base.smtp_settings = {
   :user_name => SEND_GRID_CONFIG[:USER_NAME],
   :password => SEND_GRID_CONFIG[:PASSWORD],
   :domain => SEND_GRID_CONFIG[:DOMAIN],
   :address => 'smtp.sendgrid.net',
   :port => 587,
   :authentication => :plain,
   :enable_starttls_auto => true
}